package fr.unistra.graphique;

import fr.unistra.metier.Blanc;
import fr.unistra.metier.Noir;
import fr.unistra.metier.Pion;

/**
 * classe g�rant l'interface graphique
 * 
 * @author Philippe McMahon
 *
 */
public class Affiche 
{
	
	/**
	 * Affichage du damier en ascii
	 * 
	 * @param plateau tableau � deux dimension contenant l'�tat du jeu
	 * @return void
	 */
	static public void afficheAscii(Pion plateau[][])
	{
		int i, j;
		System.out.println("Tableau: ");
		System.out.println("  1  2  3  4  5  6  7  8  9");
		for (i = 0; i < 9; i++)
		{ System.out.print(""+(i+1)+"");
			for (j = 0; j < 9; j++)
			{
				if(plateau[i][j] instanceof Noir)
					System.out.print("[O]");
				else if(plateau[i][j] instanceof Blanc)
					System.out.print("[X]");
				else
					System.out.print("[ ]");
			}
			System.out.println("");
		}
	}

}
