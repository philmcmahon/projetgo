package fr.unistra.graphique;
import javax.swing.*;
import fr.unistra.metier.Tableau;
import java.awt.*;
import java.awt.event.*;

/**
 * Classe contenant l'interface graphique
 * @author phil
 *
 */
public class Interface implements Runnable, ActionListener
{
	
	Board board;
	// le jeu
	Tableau t;
	private JButton fin;
	private JButton nouveauJeu;
	private JButton annulerCoup;
	
	// jeu info
	private JLabel pNoir;
	private JLabel pBlanc;
	private JLabel winner;
	private JLabel pointsNoir;
	private JLabel pointsBlanc;
	private JLabel terrNoir;
	private JLabel terrBlanc;
	private JLabel prochainAJouer;
	
	private JLabel valPNoir;
	private JLabel valPBlanc;
	private JLabel valWinner;
	private JLabel valPointsNoir;
	private JLabel valPointsBlanc;
	private JLabel valTerrNoir;
	private JLabel valTerrBlanc;
	private JLabel valProchainAJouer;
	
	Box gameBox = gameInfo();
	Box endGameBox = finInfo();
	/**
	 * run() - appele par 'invokeLater' dans main
	 */
	public void run()
	{
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(elements());
		
		// affiche le fenetre
		frame.pack();
		frame.setLocationByPlatform(true);
		frame.setVisible(true);
	}
	
	public Interface(Tableau leTableau)
	{
		t = leTableau;
		board = new Board(t, this);
	}
	
	/**
	 * Triggered when a button is pressed
	 */
	public void actionPerformed(ActionEvent e)
	{
		String action = e.getActionCommand();
		if (action.equals("fin"))
		{
			t.finDeJou();
			updateSideBar();
		}
		else if (action.equals("new"))
		{
			t.nouveauJeu(false);
			board.repaint();
			updateSideBar();
			gameBox.setVisible(true);
			endGameBox.setVisible(false);
		}
		else if (action.equals("undo"))
		{
			t.annulerCoup();
			updateSideBar();
			board.repaint();
		}
	}
	
	/**
	 * update sidebar info/buttons
	 */
	public void updateSideBar()
	{
		String pAJ;
		if (t.getCoupCompte() > 0)
			annulerCoup.setEnabled(true);
		else 
			annulerCoup.setEnabled(false);
		valPNoir.setText("" + t.getPNoir());
		valPBlanc.setText(""+ t.getPBlanc());
		valTerrNoir.setText("" + t.getTNoir() );
		valTerrBlanc.setText("" + t.getTBlanc());
		if (t.getProchainAJouer() == 'n')
			pAJ = "Noir";
		else
			pAJ = "Blanc";
		valProchainAJouer.setText(pAJ + " " + t.getCoupCompte() );
		if (t.getGameFini())
		{
			setWinner();
			gameBox.setVisible(false);
			endGameBox.setVisible(true);
		}
	}
	
	/**
	 * Creer bouton
	 * @param nom
	 * @param commande
	 * @return
	 */
    JButton makeButton(String name, String command)
    {
        JButton newButton = new JButton(name);
        newButton.setAlignmentX(JComponent.CENTER_ALIGNMENT);
        newButton.addActionListener(this);
        newButton.setActionCommand(command);
        return newButton;
    }


	/**
	 * Tous les boutons utiles ici (nouveau jeu, annuler etc.)
	 * @return
	 */
	Box boutons()
	{
		Box box = Box.createVerticalBox();
		box.setAlignmentY(Component.TOP_ALIGNMENT);
		//box.setPreferredSize(new Dimension(100, 300));
       // box.setMinimumSize(new Dimension(50, 300));
        annulerCoup = makeButton("Annuler", "undo");
        annulerCoup.setEnabled(false);
        nouveauJeu = makeButton("Nouveau\n Jeu", "new");
        fin = makeButton("Fin", "fin");
        box.add(nouveauJeu);
        box.add(annulerCoup);
        box.add(fin);
		return box;
	}

	JLabel makeLabel(String text)
	{
		return new JLabel(text);
	}

	Box infoBox(JLabel desc, JLabel data)
	{
		Box box = Box.createHorizontalBox();
		box.add(desc);
		box.add(data);
		return box;
	}
	
	void setWinner()
	{
		String v;
		if (t.getVainqueur() == 'n')
			v = "Noir";
		else if (t.getVainqueur() == 'b')
			v = "Blanc";
		else
			v = "Egalite";
		valPointsBlanc.setText("" + t.getPointsBlanc() );
		valPointsNoir.setText("" + t.getPointsNoir() );
		valWinner.setText(v+"");
	}
	
	Box finInfo()
	{
		Box box = Box.createVerticalBox();
		box.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		JLabel heading = makeLabel("Jeu Fini!");
		Font newLabelFont=new Font(heading.getFont().getName(),Font.BOLD,20);  
		heading.setFont(newLabelFont);
		//heading.setHorizontalTextPosition(JLabel.RIGHT);
	//	heading.setHorizontalAlignment(JLabel.RIGHT);
//		heading.setVerticalAlignment(JLabel.BOTTOM);

		winner = makeLabel(" a gagne!");
		valWinner = makeLabel("- ");
		pointsBlanc = makeLabel("Points Blanc: ");
		pointsNoir = makeLabel("Points Noir: ");
		valPointsBlanc = makeLabel("");
		valPointsNoir = makeLabel("" );
		
		box.add(heading);
		box.add(infoBox(pointsNoir, valPointsNoir));
		box.add(infoBox(pointsBlanc, valPointsBlanc));
		box.add(infoBox(valWinner, winner));
		
		return box;
	}
	
	Box gameInfo()
	{
		Box box = Box.createVerticalBox();
		box.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		//box.setPreferredSize(new Dimension(130, 200));
		pNoir = makeLabel("Prisoners Noir: ");
		pBlanc = makeLabel("Prisoners Blanc: ");
		terrNoir = makeLabel("Territoire Noir: ");
		terrBlanc= makeLabel("Territoire Blanc: ");
		prochainAJouer = makeLabel("Prochain A Jouer: ");
		valPNoir = makeLabel("0");
		valPBlanc = makeLabel("0");
	    valTerrNoir = makeLabel("0");
		valTerrBlanc = makeLabel("0");
		valProchainAJouer = makeLabel("Noir 1");
		box.add(infoBox(pNoir, valPNoir));
		box.add(infoBox(pBlanc, valPBlanc));
		box.add(infoBox(terrNoir, valTerrNoir));
		box.add(infoBox(terrBlanc, valTerrBlanc));
		box.add(infoBox(prochainAJouer, valProchainAJouer));
		return box;
	}
	
	Box sideBar()
	{
		Box box = Box.createVerticalBox();
		box.setPreferredSize(new Dimension(150, 100));
		box.setAlignmentY(Component.BOTTOM_ALIGNMENT);
		box.add(gameBox);
		box.add(endGameBox);
		endGameBox.setVisible(false);
		box.add(boutons());
		return box;
	}
	
	/**
	 * 
	 * @return box qui contient board
	 */
	Box board()
	{
		Box box = Box.createVerticalBox();
		box.add(board); 
		return box;
	}
	
	/**
	 * Ajouter le board et les boutons a le frame
	 * @return box qui contienne les elements
	 */
	Box elements()
	{
		Box box = Box.createHorizontalBox();
		box.add(board());
		box.add(sideBar());
		return box;
	}
	
	
}
