package fr.unistra.graphique;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import fr.unistra.metier.*;
import fr.unistra.graphique.Interface;
import java.lang.ClassLoader;
import javax.imageio.*;
import java.awt.image.*;
import java.io.*;
/**
 * Classe pour le representation graphique du plateau
 * @author phil
 *
 */
public class Board extends JPanel implements MouseListener, MouseMotionListener {

	// tableau
	Tableau t;
	// Interface
	Interface i;

	// board dimensions
	private int boardOffset = 20;
	private int taille = 340;
	private int placesOffset = 10;
	private int placeSize = 39;

	// mouse limits
	private int minimumCapture = boardOffset+placesOffset-(placeSize/2);
	private int maximumCapture = boardOffset+placesOffset+(8*placeSize)+(placeSize/2);
	
	//mouse coords
	int mouseX;
	int mouseY;
	
	//images
	BufferedImage noirPion;
	BufferedImage blancPion;
	//String path = getClass().getClassLoader().getResource(".").getPath();



	public Board(Tableau leTableau, Interface lInterface)
	{
		this.setPreferredSize(new Dimension(taille+boardOffset*2, taille+boardOffset*2));
		this.setMinimumSize(new Dimension(taille, taille));
		addMouseListener(this);
		addMouseMotionListener(this);
		t = leTableau;
		i = lInterface;
		mouseX = -1;
		mouseY = -1;
		try
		{
			noirPion = ImageIO.read(new File("noir.png"));
			blancPion = ImageIO.read(new File("blanc.png"));
			
		}
		catch (IOException e)
		{
			System.err.println("Image exception: " + e.getMessage());
		}
	}


	/**
	 * fonction remplace 'paintComponent' dans JPanel
	 */
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		drawBoard(g);
	}

	/**
	 * Paint le plateau
	 * @param g component graphique du JPanel
	 */
	void drawBoard(Graphics g)
	{
		int change;
		Graphics2D boardGraphics = (Graphics2D)g;
		boardGraphics.setColor(new Color(239, 169, 104));
		boardGraphics.fillRect(boardOffset, boardOffset, taille, taille);
		boardGraphics.setColor(Color.black);

		for (int i = 0; i < 9; i++)
		{
			change = boardOffset+placesOffset + i* placeSize;
			boardGraphics.drawLine(change, boardOffset+placesOffset,
					change, boardOffset+placesOffset+(placeSize*8));
			boardGraphics.drawLine(boardOffset+placesOffset, change,
					boardOffset+placesOffset+(placeSize*8), change);
		}

		for (int i = 0; i < 9; i ++)
		{
			for (int j = 0; j < 9; j++)
			{
				Pion p = t.getPlateau()[i][j];
				int xCoord = boardOffset+placesOffset+(i*placeSize) - (placeSize/4);
				int yCoord = boardOffset+placesOffset+(j*placeSize) - (placeSize/4);
				if (p instanceof Noir)
				{
					boardGraphics.setColor(Color.blue);
					//boardGraphics.fillOval(xCoord, yCoord, placeSize/2, placeSize/2);
					boardGraphics.drawImage(noirPion,xCoord, yCoord, null );
				}
				else if (p instanceof Blanc)
				{
					boardGraphics.setColor(Color.white);	
					//boardGraphics.fillOval(xCoord, yCoord, placeSize/2, placeSize/2);
					boardGraphics.drawImage(blancPion,xCoord, yCoord, null );
				}
			}
		}
		// mouse location square
		if (mouseX != -1 && mouseY != -1)
		{
			boardGraphics.setColor(new Color(139, 69, 19));
			int iXCoord = boardOffset+placesOffset+(mouseX*placeSize) - (placeSize/6);
			int iYCoord = boardOffset+placesOffset+(mouseY*placeSize) - (placeSize/6);
			boardGraphics.drawRect(iXCoord, iYCoord, placeSize/3, placeSize/3);
		}
	}


	/**
	 * When mouse clicked
	 */
	public void mouseClicked(MouseEvent e) 
	{
		if (!t.getGameFini())
		{
			// si mouse sur le tableau
			if ( e.getX() > minimumCapture && e.getX() <  maximumCapture &&
					e.getY() > minimumCapture && e.getY() < maximumCapture)
			{
				int xCoord = (e.getX() - minimumCapture)/placeSize;
				int yCoord = (e.getY() - minimumCapture)/placeSize;
				t.insererPion(xCoord, yCoord, t.getProchainAJouer());
				// if first move, update interface buttons
				i.updateSideBar();
				repaint();
			}
		}
	}
	/**
	 * When mouse moved (for location square)
	 */
	public void mouseMoved(MouseEvent e)
	{
		//t.print(mouseX + " " + mouseY);
		if (!t.getGameFini())
		{
			// si mouse sur le tableau
			if ( e.getX() > minimumCapture && e.getX() <  maximumCapture &&
					e.getY() > minimumCapture && e.getY() < maximumCapture)
			{
				mouseX = (e.getX() - minimumCapture)/placeSize;
				mouseY = (e.getY() - minimumCapture)/placeSize;
				repaint();
			}
			else
			{
				mouseX = -1;
				mouseY = -1;
			}
			
		}
		else
		{
			mouseX = -1;
			mouseY = -1;
		}
		
	}
	
	public void mouseDragged(MouseEvent e)
	{
		
	}


	public void mousePressed(MouseEvent e) 
	{

	}

	public void mouseReleased(MouseEvent e) 
	{

	}

	public void mouseEntered(MouseEvent e) 
	{

	}

	public void mouseExited(MouseEvent e) 
	{

	}

}
