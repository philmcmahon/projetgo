package fr.unistra.metier;

/**
 * Pion classe
 * @author phil
 *
 */
public class Pion {

	int x;
	int y;
	int liberte;
	boolean prise;
	boolean chainChecked;
	boolean suicideChecked;
	
	public Pion()
	{
		x=-1;
		y=-1;
		prise = false;
		chainChecked = false;
		liberte=4;
	}
	
	public Pion(int x,int y)
	{
		this.x=x;
		this.y=y;
		prise = false;
		chainChecked = false;
		liberte=4;
	}
	
	public void setPrise(boolean b)
	{
		prise = b;
	}
	
	public void setChainChecked(boolean b)
	{
		chainChecked = b;
	}
	
	public void setSuicideChecked(boolean b)
	{
		suicideChecked = b;
	}
	
	public boolean getPrise() {return prise;}
	public boolean getChainChecked() {return chainChecked;}
	public boolean getSuicideChecked() {return suicideChecked;}

	
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	
	
}
