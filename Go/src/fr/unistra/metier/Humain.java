package fr.unistra.metier;

import java.util.Scanner;

/**
 *  Classe pour representer un joueur humain
 * 
 */
public class Humain extends Joueur {

	public Humain(char joueurCouleur) {
		super(joueurCouleur);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Fonction pour demander un coup a le joueur
	 */
	public Coup getCoup()
	{
		int x=-1, y=-1;
		boolean gotCoord = false;
		Scanner scan = new Scanner(System.in);
		String input;
		while (gotCoord == false)
		{
			System.out.print("X coord: ");
			input = scan.nextLine();
			System.out.println();

			try
			{
				x = Integer.parseInt(input);
				gotCoord = true;
			}
			catch (NumberFormatException e)
			{
				System.err.println("Enter numero 1-9 svp");
			}
			if (x > 9 || x < 1)
			{
				System.err.println("Enter numero 1-9 svp");
				gotCoord = false;
			}
		}
		gotCoord = false;

		while (gotCoord == false)
		{
			System.out.print("Y coord: ");
			input = scan.nextLine();
			System.out.println();

			try
			{
				y = Integer.parseInt(input);
				gotCoord = true;
			}
			catch (NumberFormatException e)
			{
				System.err.println("Enter numero 1-9 svp");
			}
			if (y > 9 || y < 1)
			{
				System.err.println("Enter numero 1-9 svp");
				gotCoord = false;
			}
		}
		scan.close();

		return new Coup(x-1, y-1, couleur);
	}

}
