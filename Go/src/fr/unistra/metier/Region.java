package fr.unistra.metier;

/**
 * Classe pour stockage les detailes d'un region/territoire
 * @author phil
 *
 */
public class Region {
	
	public enum TerrType
	{
		Noir, Blanc, Neutrale, Rien;
	}
	
	TerrType regType;
	int taille;
	
	public Region()
	{
		regType = TerrType.Rien;
		taille = 0;
	}
	
	/**
	 * 
	 * @param t - type de la pion ajouter au region
	 */
	public void updateType(TerrType t)
	{
		if (t != regType && regType != TerrType.Neutrale && t != TerrType.Rien)
		{
			if (regType == TerrType.Rien)
				regType = t;
			else
				regType = TerrType.Neutrale;
		}
			
	}
	
	public TerrType getType() { return regType;}
	public int getTaille() {return taille; }
	public void incTaille() {taille++; }

}
