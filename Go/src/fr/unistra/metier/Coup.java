package fr.unistra.metier;

/**
 * Classe pour le stockage d'un nouveau coup
 */
public class Coup {
	
	int x, y;
	char couleur;
	
	public Coup(int coupX, int coupY, char coupCouleur)
	{
		x = coupX;
		y = coupY;
		couleur = coupCouleur;
	}
	
	public int getX()
	{
		return x;
	}
	
	public int getY()
	{
		return y;
	}
	
	public char getCouleur()
	{
		return couleur;
	}

}
