package fr.unistra.metier;

import java.io.*;

/**
 * Class handles input/output file operations
 * @author phil
 *
 */
public class FichierIO {
	
	// move ouput stream
	FileWriter fstream;
	BufferedWriter out;
	// board output stream
	FileWriter boardStream;
	BufferedWriter boardOut;
	
	// used for updating prisoner info
	public boolean boardDataPret;
	int numeroPrisonerBlanc;
	int numeroPrisonerNoir;
	
	public FichierIO()
	{
		boardDataPret = false;
		try
		{
			fstream = new FileWriter("coups.go");
			out = new BufferedWriter(fstream);
			// delete contents of 'plateau.go'
			boardStream = new FileWriter("plateau.go");
			boardOut = new BufferedWriter(boardStream);
			boardOut.write("");
			boardOut.close();
			
		}
		catch (Exception e)
		{
			System.err.println("File open Error: " + e.getMessage());
		}
	}
	
	/**
	 * Delete boards from plateau.go after a rollback
	 * @param endLine
	 */
	void deleteFrom(int endLine)
	{
		try
		{
			File inputFile = new File("plateau.go");
			FileInputStream deleteInStream= new FileInputStream(inputFile);
			DataInputStream dataIn = new DataInputStream(deleteInStream);
			BufferedReader dbr = new BufferedReader(new InputStreamReader(dataIn));
			
			File tempFile = new File("tempplateau.go");
			FileWriter deleteStream = new FileWriter(tempFile);
			BufferedWriter deleteOut = new BufferedWriter(deleteStream);

			String readLine;
			
			while ((readLine = dbr.readLine()) != null)
			{
				if (!(readLine.equals(endLine+"") ) )
					deleteOut.write(readLine+"\n");
				else
					break;
			}
			dbr.close();
			deleteOut.close();
			boolean deleteSuccess = inputFile.delete();
			boolean renameSuccess = tempFile.renameTo(inputFile);
			if (!deleteSuccess && !renameSuccess)
				System.err.println("Temp remplacement annuler");
			//System.out.println("Delete: " + deleteSuccess + " Rename: " + renameSuccess);
		}
		catch (Exception e)
		{
			System.err.println("Exception: " + e.getMessage());
		}
	}
	
	public int getPNoir()
	{
		if (boardDataPret)
			return numeroPrisonerNoir;
		else 
			return -1;
	}
	
	public int getPBlanc()
	{
		if (boardDataPret)
		{
			return numeroPrisonerBlanc;
		}
		else 
			return -1;
	}
	
	/**
	 * Write a move to file
	 * @param x - x coordinate
	 * @param y - y coordinate
	 * @param couleur - colour (noir/blanc)
	 */
	public void writeCoup(int x, int y, char couleur)
	{
		String s = x + ", " + y + " " + couleur + "\n";
		try
		{
			out.write(s);
		}
		catch (Exception e)
		{
			System.err.println("File write Error: " + e.getMessage());
		}
	}
	
	/**
	 * Closet the 'coups.go' ouput stream
	 */
	public void closeOut()
	{
		try
		{
			out.close();
		}
		catch (Exception e)
		{
			System.err.println("File close Error: " + e.getMessage());
		}
	}
	
	/**
	 * Write s to buffer out
	 * @param out write buffer
	 * @param s string to write
	 */
	public void writeBuf(BufferedWriter out, String s)
	{
		try
		{
			out.write(s);
		}
		catch (Exception e)
		{
			System.err.println("File write Error: " + e.getMessage());
		}
	}
	
	/**
	 * Roll back the board to move coupNo
	 * @param coupNo
	 * @return
	 */
	public Pion[][] rollBackBoard(int coupNo)
	{
		DataInputStream plateauIn;
		BufferedReader br;
		String readLine;
		Pion[][] nouveauPlateau = new Pion[9][9];
		try
		{
			FileInputStream instream = new FileInputStream("plateau.go");
			plateauIn = new DataInputStream(instream);
			br = new BufferedReader(new InputStreamReader(plateauIn));
			boardDataPret = true;
			while ((readLine = br.readLine()) != null)
			{
				if (readLine.equals(""+coupNo))
				{
					// prisoner info
					readLine = br.readLine();
					numeroPrisonerNoir = Integer.parseInt(readLine.substring(12));
					readLine = br.readLine();
					numeroPrisonerBlanc = Integer.parseInt(readLine.substring(12));
					for (int i = 0; i <9; i++)
					{
						readLine = br.readLine();
						for (int j = 0; j < 9; j++)
						{
							if (readLine.charAt(j) == '-')
								nouveauPlateau[i][j] = null;
							else if (readLine.charAt(j) == 'n')
								nouveauPlateau[i][j] = new Noir(i, j);
							else if (readLine.charAt(j) == 'b')
								nouveauPlateau[i][j] = new Blanc(i, j);
						}
					}
					br.close();
					break;
				}
			}
		}
		catch (Exception e)
		{
			System.err.println("rollbackfilexception: " + e.getMessage());
		}
		// update "plateau.go" file
		deleteFrom(coupNo+1);
		return nouveauPlateau;
	}
	
	/**
	 * Write current board and prisoner variables to file
	 * @param plateau
	 * @param coupCompte
	 * @param pNoir
	 * @param pBlanc
	 */
	public void writeBoard(Pion plateau[][], int coupCompte, int pNoir, int pBlanc)
	{
		try
		{
			boardStream = new FileWriter("plateau.go", true);
			boardOut = new BufferedWriter(boardStream);
			writeBuf(boardOut, coupCompte + "\n");
			writeBuf(boardOut, "Prisoner n: " + pNoir + "\n");
			writeBuf(boardOut, "Prisoner b: " + pBlanc + "\n");
			for (int i = 0; i < 9; i++)
			{
				for (int j = 0; j < 9; j++)
				{
					if (plateau[i][j] == null)
						writeBuf(boardOut, "-");
					else if (plateau[i][j] instanceof Noir)
						writeBuf(boardOut, "n");
					else if (plateau[i][j] instanceof Blanc)
						writeBuf(boardOut, "b");
					else
						System.err.println("Unrecognised pion");	
				}
				boardOut.newLine();
			}
			writeBuf(boardOut, coupCompte+"END");
			boardOut.newLine();
			boardOut.close();
		}
		catch (Exception e)
		{
			System.err.println("File open Error: " + e.getMessage());
		}
		
	}
}

