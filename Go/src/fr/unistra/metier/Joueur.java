package fr.unistra.metier;

public class Joueur {
	char couleur;
	
	public Joueur()
	{
		couleur=' ';
	}
	
	/**
	 * @param couleurJoueur - le couleur du pion pour ce joueur
	 */
	public Joueur(char couleurJoueur)
	{
		couleur=couleurJoueur;
	}
}
