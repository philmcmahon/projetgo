package fr.unistra.metier;
/**
 * Classe contenant toutes les informations du jeu en cours
 * 
 * @author Julien Rous�
 *
 */

import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
public class Tableau
{
	
	Pion[][] plateau;
	// flag pour di si la place est deja compte
	Boolean[][] tCompte = new Boolean[9][9];
	
	int koX;
	int koY;
	
	FichierIO io;
	
	int numeroPrisonerNoir;
	int numeroPrisonerBlanc;
	
	char vainqueur; 
	boolean gameFini;
	
	int territoireNoir;
	int territoireBlanc;
	int pointsNoir;
	int pointsBlanc;
	
	int coupCompte;
	
	char prochainAJouer;
	
	/**
	 * Constructeur vide
	 */
	public Tableau()
	{
		plateau=new Pion[9][9];
		resetVars();
	}
	/**
	 * Constructeur qui prend en parametre un char
	 * 
	 * @param quiCommence : le joueur qui commence
	 */
	public Tableau(char quiCommence)
	{
		plateau=new Pion[9][9];
		prochainAJouer = quiCommence;
		resetVars();
	}
	
	public void resetVars()
	{
		territoireNoir = 0;
		gameFini = false;
		koX = -1;
		koY = -1;
		io = new FichierIO();
		territoireBlanc = 0;
		numeroPrisonerNoir = 0;
		numeroPrisonerBlanc = 0;
		coupCompte = 0;
		io.writeBoard(plateau, coupCompte, numeroPrisonerNoir, numeroPrisonerBlanc);
	}
	
	public void nouveauJeu(boolean jeuFini)
	{
		resetVars();
		if (jeuFini)
			prochainAJouer = vainqueur;
		else
			prochainAJouer = 'n';
		for (int i = 0; i < 9; i++)
			Arrays.fill(plateau[i], null);
	}
	
	public int getCoupCompte()
	{
		return coupCompte;
	}

	/**
	 * fonction pour inserer un pion
	 * @param x abcisse des coordonn�es
	 * @param y ordonn� des coordonn�es
	 * @param couleur couleur du pion � inserer(ie blanc ou noir)
	 * @return true si le pion est pos�, false sinon
	 */
	public boolean insererPion(int x, int y, char couleur)
	{
		// reset ko variables if move is elsewhere
		if (x != koX && y != koY)
		{
			koX = -1;
			koY = -1;
		}

		if (x >= 0 && y >= 0 && x < 9 && y < 9)
		{
			if (plateau[x][y] == null )
			{
				if (couleur == 'n')
				{
					plateau[x][y] = new Noir(x, y);
					if(!reglesAvantCoup(x, y))
					{
						plateau[x][y] = null;
						return false;
					}
					prochainAJouer = 'b';
				}
				else if (couleur == 'b')
				{
					plateau[x][y] = new Blanc(x, y);
					if(!reglesAvantCoup(x, y))
					{
						plateau[x][y] = null;
						return false;
					}
					prochainAJouer = 'n';
				}
				coupCompte++;
				io.writeCoup(x, y, couleur);
				reglesApresCoup(x, y);
				io.writeBoard(plateau, coupCompte, numeroPrisonerNoir, numeroPrisonerBlanc);
				return true;
			}
			else
			{
				// pion deja la
				System.err.println("Pion deja la");
				return false;
			}
		}
		else
		{
			System.err.println("Coordonees invalide");
			return false;
		}
	}
	
	public void annulerCoup()
	{
		plateau = io.rollBackBoard(coupCompte - 1);
		coupCompte--;
		numeroPrisonerNoir = io.getPNoir();
		numeroPrisonerBlanc = io.getPBlanc();
		io.boardDataPret = false;
		if (prochainAJouer == 'n')
			prochainAJouer = 'b';
		else
			prochainAJouer = 'n';
		compteTerritoire();
	}
	
	/**
	 * 
	 * @param p1 
	 * @param p2
	 * @return vrai si p1 et p2 sont le meme couleur
	 */
	public boolean memeCouleur(Pion p1, Pion p2)
	{
		return p1.getClass().equals(p2.getClass());
	}
	
	/**
	 * Prise pion
	 * @param p
	 */
	void prise(Pion p)
	{
		if (p instanceof Noir)
			numeroPrisonerNoir++;
		else
			numeroPrisonerBlanc++;
		plateau[p.getX()][p.getY()] = null;

		System.out.println("Pion " + p.getX() +", " + p.getY() + " prise!");
		updateLiberte(p.getX(), p.getY(), 1);
	}
	
	/**
	 * Prise les pions qui sont marque
	 * @param prise
	 */
	void fairePrise(boolean prise)
	{
		int premierX = -1;
		int premierY = -1;
		int pNoirAvant = numeroPrisonerNoir;
		int pBlancAvant = numeroPrisonerBlanc;
		boolean premierPrise = true;
		for (int i = 0; i < 9; i++)
		{
			for (int j = 0; j < 9; j++)
			{
				if (plateau[i][j] != null)
				{
					if (prise && plateau[i][j].getPrise())
					{
						prise(plateau[i][j]);
						if (premierPrise)
						{
							premierX = i;
							premierY = j;
							premierPrise = false;
						}
					}
					else
					{
						plateau[i][j].setPrise(false);
						plateau[i][j].setChainChecked(false);
					}
				}
			}
		}
		// set ko variables
		if (numeroPrisonerNoir - pNoirAvant == 1 || numeroPrisonerBlanc - pBlancAvant == 1
				&& premierX != -1 && premierY != -1)
		{
			print("koupdated: " + premierX + " " + premierY);
			koX = premierX;
			koY = premierY;
		}
	}
	
	/**
	 * Fonction pour le regle de prise (recursif)
	 * @param x abcisse
	 * @param y ordon�e
	 */
	public boolean regleDePrise(int x, int y)
	{
		plateau[x][y].setChainChecked(true);
		Pion p = plateau[x][y];
		Pion pg = null, pd = null, ph = null, pb = null;

		if (x!=0) pg = plateau[x-1][y];
		if (x!=8) pd = plateau[x+1][y];
		if (y!=0) ph = plateau[x][y-1];
		if (y!=8) pb = plateau[x][y+1];
		//System.out.println(p.getClass()  + " liberte " + p.liberte + " x: " + x + " y: " + y);
		if (plateau[x][y].liberte == 0 && 
				(x == 0 || pg.getChainChecked() || !memeCouleur(pg, p) || regleDePrise(x-1, y)) &&
				(x == 8 || pd.getChainChecked() || !memeCouleur(pd, p) || regleDePrise(x+1, y)) &&
				(y == 0 || ph.getChainChecked() || !memeCouleur(ph, p) || regleDePrise(x, y-1)) &&
				(y == 8 || pb.getChainChecked() || !memeCouleur(pb, p) || regleDePrise(x, y+1)))
		{
			plateau[x][y].setPrise(true);		
			return true;
		}
		return false;
	}
	
	public void checkPriseRegle(int x, int y)
	{
		//print("X:" + x + " Y:" + y);
		if(x > 0 && plateau[x-1][y]!=null && regleDePrise(x-1, y)) fairePrise(true); else fairePrise(false);
		if(x < 8 && plateau[x+1][y]!=null && regleDePrise(x+1, y)) fairePrise(true); else fairePrise(false);
		if(y > 0 && plateau[x][y-1]!=null &&regleDePrise(x, y-1)) fairePrise(true); else fairePrise(false);
		if(y < 8 && plateau[x][y+1]!=null && regleDePrise(x, y+1)) fairePrise(true); else fairePrise(false);
	}

	/**
	 * Fonction pour mise en jour le tableau apres un nouveau coup
	 * @param x abcisse
	 * @param y ordonn�e
	 */
	public void reglesApresCoup(int x, int y)
	{
		//updateLiberte(x, y, -1);
		plateau[x][y].liberte=compteLiberte(x, y);
		checkPriseRegle(x,y);
		compteTerritoire();
	}
	
	/**
	 * Fonction pour tester si un coup est valide avec les regles du go
	 * @param x abcisse
	 * @param y ordonn�e
	 * @return true si le coup est valide
	 */
	public boolean reglesAvantCoup(int x, int y)
	{
		// regle ko
		if (koX != -1 && koY != -1 && x == koX && y == koY)
		{
			print("Ko");
			return false;
		}
		// regle suicide
		plateau[x][y].liberte=compteLiberte(x, y);
		updateLiberte(x, y, -1);
		if (regleDePrise(x, y))
		{
			if (!regleDePrise(x-1, y) && !regleDePrise(x+1, y) &&
					!regleDePrise(x, y-1) && !regleDePrise(x, y+1))
			{
				print("Suicide");
				fairePrise(false);
				updateLiberte(x, y, 1);
				return false;
			}
		}
		fairePrise(false);
		return true;//checkSuicide(x,y);
	}

	
	/**
	 * Compte les libert�es d'une case indiqu� par ses coordonn�es x,y
	 * @param x abcisse
	 * @param y ordonn�e
	 * @return le nombre de libert� de la case (compris entre 0 et 4)
	 */
	public int compteLiberte(int x,int y)
	{
		int i=4;
		//on regarde chaque voisin direct, et si la case est non vide on decremente le compteur de liberte
		if(x-1<0 || plateau[x-1][y]!=null)i--;
		if(x+1>8 || plateau[x+1][y]!=null)i--;
		if(y-1<0 || plateau[x][y-1]!=null)i--;
		if(y+1>8 || plateau[x][y+1]!=null)i--;
		return i;
	}
	
	/**
	 * d�cremente les libert�s des toutes les cases alentours de 1
	 * @param x abcisse
	 * @param y ordonn�e
	 */
	public void updateLiberte(int x,int y, int change)
	{
		if(x > 0 && plateau[x-1][y]!=null)
			plateau[x-1][y].liberte += change;
		if(x < 8 && plateau[x+1][y]!=null)
			plateau[x+1][y].liberte += change;
		if(y > 0 && plateau[x][y-1]!=null)
			plateau[x][y-1].liberte += change;
		if(y < 8 && plateau[x][y+1]!=null)
			plateau[x][y+1].liberte += change;
		return ;
	}
	


	/**
	 * Getter de l'�tat du jeu 
	 * 
	 * @return un tableau contenant l'etat du jeu actuel
	 */
	public Pion[][] getPlateau()
	{
		return plateau;
	}
	
	/**
	 * Chercher dans tous la region du espace x, y
	 * Ajouter les detailles a Region r
	 * @param x
	 * @param y
	 * @param r
	 */
	public void floodFillRegion(int x, int y, Region r)
	{
		Pion p = plateau[x][y];
		if (p != null )
			return;
		else
		{
			tCompte[x][y] = true;
			r.updateType(pionRegion(x,y));
			r.incTaille();
			if (x != 0 && !tCompte[x-1][y])
				floodFillRegion(x-1, y, r);
			if (x != 8 && !tCompte[x+1][y])
				floodFillRegion(x+1, y, r);
			if (y != 0 && !tCompte[x][y-1])
				floodFillRegion(x, y-1, r);
			if (y != 8 && !tCompte[x][y+1])
				floodFillRegion(x, y+1, r);
		}
	}
	
	/**
	 * Trouver si le pion (x,y) est dans un noir/blanc/neutral region
	 * @param x
	 * @param y
	 * @return
	 */
	public Region.TerrType pionRegion(int x, int y)
	{
		int compteNoir, compteBlanc;
		Pion voisins[] = new Pion[4];
		compteNoir = 0;
		compteBlanc = 0;
		if(x != 0 && plateau[x-1][y] != null)
			voisins[0] = plateau[x-1][y];
		if (x != 8 && plateau[x+1][y] != null)
			voisins[1] = plateau[x+1][y];
		if(y != 0 && plateau[x][y-1] != null)
			voisins[2] = plateau[x][y-1];
		if (y != 8 && plateau[x][y+1] != null)
			voisins[3] = plateau[x][y+1];
		for (int k = 0; k < 4; k++)
		{
			if (voisins[k] != null)
			{
				if (voisins[k] instanceof Noir)
					compteNoir++;
				else if (voisins[k] instanceof Blanc)
					compteBlanc++;
			}
		}
		if (compteNoir > 0 && compteBlanc > 0)
			return Region.TerrType.Neutrale;
		else if (compteNoir >0 && compteBlanc == 0)
			return Region.TerrType.Noir;
		else if (compteNoir == 0 && compteBlanc > 0)
			return Region.TerrType.Blanc;
		else
			return Region.TerrType.Rien;
	}

	/**
	 * Compte le territoires de blanc et noir
	 */
	public void compteTerritoire()
	{
		int i;
		territoireNoir = 0;
		territoireBlanc = 0;
		for (i = 0; i < 9; i++)
			Arrays.fill(tCompte[i], Boolean.FALSE);
		List<Region> regions = new ArrayList<Region>();
		for (i = 0; i < 9; i++)
		{
			for (int j = 0; j <9; j++)
			{
				if (plateau[i][j] == null && !tCompte[i][j])
				{
					regions.add(new Region());
					floodFillRegion(i, j, regions.get(regions.size()-1));
				}
			}
		}
		for (i = 0; i < regions.size(); i++)
		{
			if (regions.get(i).getType() == Region.TerrType.Noir)
				territoireNoir += regions.get(i).getTaille();
			else if (regions.get(i).getType() == Region.TerrType.Blanc)
				territoireBlanc += regions.get(i).getTaille();   
				
			//print("Region " + i + " taille: " + regions.get(i).getTaille() + " de " + regions.get(i).getType());
		}
		//print("Noir territoire: " + territoireNoir + " Blanc territoire " + territoireBlanc);
	}
	
	/**
	 * Compte les points, trouver qui a gagne
	 */
	public void finDeJou()
	{
		gameFini = true;
		compteTerritoire();
		pointsNoir = territoireNoir - numeroPrisonerNoir;
		pointsBlanc = territoireBlanc - numeroPrisonerBlanc;
		if (pointsNoir > pointsBlanc)
			vainqueur = 'n';
		else if (pointsNoir < pointsBlanc)
			vainqueur = 'b';
		else
			vainqueur = 'e';
		io.closeOut();
	}
	
	public void print(String x)
	{
		System.out.println(""+ x);
	}
	
	public char getProchainAJouer()
	{
		return prochainAJouer;
	}
	
	public int getPNoir() { return numeroPrisonerNoir; }
	public int getPBlanc() { return numeroPrisonerBlanc; }
	public int getTNoir() { return territoireNoir; }
	public int getTBlanc() { return territoireBlanc; }
	public char getVainqueur() { return vainqueur; }
	public int getPointsNoir() { return pointsNoir; }
	public int getPointsBlanc() { return pointsBlanc; }
	public boolean getGameFini() { return gameFini ; }

}
