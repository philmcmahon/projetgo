package fr.unistra.metier;

import static org.junit.Assert.*;

import org.junit.Test;

public class PionTest {

	@Test
	public void constructeurVideTest()
	{
		Pion p=new Pion();
		assertTrue(p.x==-1);
		assertTrue(p.y==-1);
	}
	
	@Test
	public void constructeurTest()
	{
		Pion p=new Pion(4,2);
		assertTrue(p.x==4);
		assertTrue(p.y==2);
	}
}
