package fr.unistra.metier;

import static org.junit.Assert.*;

import org.junit.Test;

public class NoirTest {

	@Test
	public void constructeurVideTest()
	{
		Noir n=new Noir();
		assertTrue(n.x==-1);
		assertTrue(n.y==-1);
	}
	
	@Test
	public void constructeurTest()
	{
		Noir n=new Noir(3,1);
		assertTrue(n.x==3);
		assertTrue(n.y==1);
	}
}
