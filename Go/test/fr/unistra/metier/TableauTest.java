package fr.unistra.metier;

import static org.junit.Assert.*;

import org.junit.Test;

import fr.unistra.graphique.Affiche;
import fr.unistra.metier.Tableau;

public class TableauTest {

	@Test
	public void constructeurVideTest()
	{
		Tableau t = new Tableau();
		assertTrue(t.getPlateau()[3][3] == null);
		assertTrue(t.getPlateau()[2][6] == null);
		
	}
	
	@Test
	public void insererPionTest()
	{
		Tableau t = new Tableau();
		assertTrue(t.insererPion(5, 5, 'b'));
		assertFalse(t.insererPion(5, 5, 'n'));
		assertFalse(t.insererPion(10, 10, 'b'));
		//Affiche.afficheAscii(t.plateau);
	}
	
	@Test
	public void testCompteLiberte()
	{ 	
		Tableau t = new Tableau();
		t.insererPion(2, 2, 'b');
		t.insererPion(3, 3, 'b');
		t.insererPion(1, 3, 'b');
		t.insererPion(2, 4, 'b');
		t.insererPion(2, 3, 'n');
		t.insererPion(0, 3, 'n');
		Affiche.afficheAscii(t.plateau);
		
		assertTrue(3==t.plateau[3][3].liberte);
		assertTrue(0==t.plateau[2][3].liberte);
		assertTrue(2==t.plateau[1][3].liberte);
		assertTrue(3==t.plateau[2][2].liberte);
		assertTrue(3==t.plateau[2][4].liberte);
		assertTrue(2==t.plateau[0][3].liberte);
		
		t.insererPion(8, 8, 'b');
		t.insererPion(8, 7, 'n');
		Affiche.afficheAscii(t.plateau);
		assertTrue(1==t.plateau[8][8].liberte);
		assertTrue(2==t.plateau[8][7].liberte);
	}
}
