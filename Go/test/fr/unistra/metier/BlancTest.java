package fr.unistra.metier;

import static org.junit.Assert.*;

import org.junit.Test;

public class BlancTest {
	
	@Test
	public void constructeurVideTest()
	{
		Blanc b=new Blanc();
		assertTrue(b.x==-1);
		assertTrue(b.y==-1);
	}
	
	@Test
	public void constructeurTest()
	{
		Blanc b=new Blanc(7,8);
		assertTrue(b.x==7);
		assertTrue(b.y==8);
	}
}
